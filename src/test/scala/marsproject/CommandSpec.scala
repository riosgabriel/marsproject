package marsproject

import marsproject.Commands.{ Invalid, Left, Move, Right }
import org.scalatest.{ FlatSpec, Matchers }

class CommandSpec extends FlatSpec with Matchers {
  behavior of "Command"

  it should "apply" in {
    Command('L') shouldEqual Left
    Command('R') shouldEqual Right
    Command('M') shouldEqual Move
    Command('X') shouldEqual Invalid
  }
}
