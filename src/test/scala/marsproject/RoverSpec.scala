package marsproject

import org.scalatest.{ FlatSpec, Matchers }

class RoverSpec extends FlatSpec with Matchers {

  "Rover(1, 2, North) with commands LMLMLMLMM" should "expected Rover(1, 3, North)" in {
    val plateau = Plateau(5, 5)
    val rover = Rover(1, 2, North)
    val commands = "LMLMLMLMM".toList.map(c => Command(c))

    rover.performCommand(commands, plateau.x, plateau.y) shouldEqual Rover(1, 3, North)
  }

  "For Rover(3, 3, East) with commands MMRMMRMRRM" should "expected Rover(5, 1, East)" in {
    val plateau = Plateau(5, 5)
    val rover = Rover(3, 3, East)
    val commands = "MMRMMRMRRM".toList.map(c => Command(c))

    rover.performCommand(commands, plateau.x, plateau.y) shouldEqual Rover(5, 1, East)
  }

  "For Rover(0, 1, North) with commands MMM" should "expected Rover(0, 1, North)" in {
    val plateau = Plateau(2, 2)
    val rover = Rover(0, 1, North)
    val commands = "MMM".toList.map(c => Command(c))

    rover.performCommand(commands, plateau.x, plateau.y) shouldEqual Rover(0, 2, North)
  }

  "For Rover(0, 0, West) with commands MMM" should "expected Rover(0, 0, West)" in {
    val plateau = Plateau(2, 2)
    val rover = Rover(0, 0, West)
    val commands = "MMM".toList.map(c => Command(c))

    rover.performCommand(commands, plateau.x, plateau.y) shouldEqual Rover(0, 0, West)
  }
}
