package marsproject

import org.scalatest.{ FlatSpec, Matchers }

class PlateauSpec extends FlatSpec with Matchers {
  behavior of "Plateau"

  it should "isInBoundaries" in {
    Plateau(2, 2).isInBoundaries(1, 0) should be(true)
    Plateau(2, 2).isInBoundaries(3, 0) should be(false)
    Plateau(2, 2).isInBoundaries(1, 2) should be(true)
    Plateau(2, 2).isInBoundaries(-1, 0) should be(false)
  }

}
