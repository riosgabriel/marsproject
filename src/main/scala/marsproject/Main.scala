package marsproject

import scala.io.Source
import scala.util.{ Failure, Success, Try }
import scala.util.matching.Regex

object Main extends App {

  val plateauPattern: Regex = "^(\\d)\\s(\\d)$".r
  val roverPositionPattern: Regex = "^(\\d)\\s(\\d)\\s([NEWS])$".r
  val commandsPattern: Regex = "^([MmRrLl]+)$".r

  Try(Source.fromFile("input.txt")) match {
    case Failure(exception) => println(s"An exception occurs: $exception")
    case Success(source) =>
      val lines = source.getLines()

      lines.next() match {
        case plateauPattern(x, y) =>
          val plateau = Plateau(x.toInt, y.toInt)

          lines.foreach {
            case roverPositionPattern(x, y, direction) =>
              if (plateau.isInBoundaries(x.toInt, y.toInt)) {
                val rover = Rover(x.toInt, y.toInt, CardinalDirection(direction.head))
                val commands = lines.next().toList.map(c => Command(c))

                println(rover.performCommand(commands, plateau.x, plateau.y))

              } else println(s"Rover is in a invalid state")

            case _ => println(s"Invalid Rover Data")
          }

        case _ => println(s"Invalid Plateau Data")
      }
  }
}
