package marsproject

import marsproject.Commands.{ Left, Move, Right }

case class Rover(x: Int, y: Int, direction: CardinalDirection) {

  private def left: Rover = this.copy(direction = direction.left)

  private def right: Rover = this.copy(direction = direction.right)

  private def move(maxX: Int, maxY: Int): Rover =
    this.direction match {
      case North if y + 1 <= maxY => this.copy(y = y + 1)
      case South if y - 1 >= 0 => this.copy(y = y - 1)
      case West if x - 1 >= 0 => this.copy(x = x - 1)
      case East if x + 1 <= maxX => this.copy(x = x + 1)
      case _ => this
    }

  def performCommand(commands: List[Command], maxX: Int, maxY: Int): Rover =
    commands
      .filterNot(_.isInvalid)
      .foldLeft(this) {
        case (rover, command) =>
          command match {
            case Move => rover.move(maxX, maxY)
            case Right => rover.right
            case Left => rover.left
            case _ => rover
          }
      }

  override def toString: String =
    s"$x $y $direction"

}
