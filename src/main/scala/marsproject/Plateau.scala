package marsproject

case class Plateau(x: Int, y: Int) {

  def isInBoundaries(x: Int, y: Int): Boolean =
    x >= 0 && x <= this.x && y >= 0 && y <= this.y
}
