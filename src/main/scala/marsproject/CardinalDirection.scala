package marsproject

sealed trait CardinalDirection {
  val left: CardinalDirection
  val right: CardinalDirection
}

case object North extends CardinalDirection {
  override val left: CardinalDirection = West
  override val right: CardinalDirection = East

  override def toString: String = "N"
}

case object South extends CardinalDirection {
  override val left: CardinalDirection = East
  override val right: CardinalDirection = West

  override def toString: String = "S"
}

case object West extends CardinalDirection {
  override val left: CardinalDirection = South
  override val right: CardinalDirection = North

  override def toString: String = "W"
}

case object East extends CardinalDirection {
  override val left: CardinalDirection = North
  override val right: CardinalDirection = South

  override def toString: String = "E"
}

object CardinalDirection {
  def apply(char: Char): CardinalDirection = char match {
    case 'N' => North
    case 'S' => South
    case 'W' => West
    case 'E' => East
  }
}
