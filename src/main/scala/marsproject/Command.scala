package marsproject

import marsproject.Commands.{ Invalid, Left, Move, Right }

sealed trait Command {
  def isInvalid: Boolean = this == Invalid
}

object Command {
  def apply(command: Char): Command = command match {
    case 'L' ⇒ Left
    case 'R' ⇒ Right
    case 'M' ⇒ Move
    case _   ⇒ Invalid
  }
}

object Commands {
  case object Left extends Command
  case object Right extends Command
  case object Move extends Command
  case object Invalid extends Command
}
