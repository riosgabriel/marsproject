##Mars Rover in Scala

### Installation
* [Scala](https://www.scala-lang.org/download/) - The Scala Programming Language
* [Sbt](https://www.scala-sbt.org/download.html) - Simple Build Tool

### Usage

To Run:

```
$ sbt
> run
```

To Test:
```bash
$ sbt
> test
```